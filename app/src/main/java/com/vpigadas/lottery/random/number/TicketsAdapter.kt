package com.vpigadas.lottery.random.number

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import com.vpigadas.lottery.random.number.abstracts.AbstractViewHolder
import com.vpigadas.lottery.random.number.models.GeneratedModels

class TicketsAdapter(private val listener: HoldersListener) :
    ListAdapter<GeneratedModels, AbstractViewHolder>(MyListDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AbstractViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.holder_game, parent, false)
        view.setOnClickListener { listener.onClickView(it) }
        return TicketsViewHolder(view)
    }

    fun getData(position: Int): GeneratedModels {
        return super.getItem(position)
    }

    override fun onBindViewHolder(holder: AbstractViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

}