package com.vpigadas.lottery.random.number

import android.content.Intent
import com.google.android.material.snackbar.Snackbar
import com.vpigadas.lottery.random.number.abstracts.AbstractActivity
import com.vpigadas.lottery.random.number.models.GameDefinition
import com.vpigadas.lottery.random.number.utils.AreaDefinitionSingleton
import kotlinx.android.synthetic.main.activity_third_screen.*
import kotlinx.android.synthetic.main.content_third_screen.*
import java.util.*

class ThirdScreen : AbstractActivity(), ItemListDialogFragment.Listener {

    private val listOfNumbers: MutableList<Int> = mutableListOf()
    override fun getResLayout(): Int = R.layout.activity_third_screen

    override fun initLayout() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val obj = intent.getParcelableExtra<GameDefinition>(AreaDefinitionSingleton.EXTRA_ARG_GAME)

        if (obj == null) {
            Snackbar.make(
                toolbar,
                getString(R.string.error_parcelable_obj),
                Snackbar.LENGTH_SHORT
            )
        }

        toolbar.title = obj.getName(Locale.getDefault())
        btn_num_tickets.text = getString(R.string.num_of_tickets, "${listOfNumbers.count()}")
        btn_num_tickets.setOnClickListener {
            ItemListDialogFragment.newInstance(20, listOfNumbers.lastOrNull() ?: 0)
                .show(supportFragmentManager, "ItemListDialogFragment")
        }

        btn_random_numbers.setOnClickListener {
            val intent =
                Intent(this, FourthScreen::class.java).also { it.putExtra(AreaDefinitionSingleton.EXTRA_ARG_GAME, obj) }
            startActivity(intent)
        }
    }

    override fun dismissUi() {}

    override fun onItemClicked(position: Int) {
        listOfNumbers.add(position)
        btn_num_tickets.text = getString(R.string.num_of_tickets, "${listOfNumbers.count()}")
    }
}
