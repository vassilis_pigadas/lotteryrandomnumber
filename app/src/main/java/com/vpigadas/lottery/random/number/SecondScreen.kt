package com.vpigadas.lottery.random.number

import android.content.Intent
import android.view.View
import com.vpigadas.lottery.random.number.abstracts.AbstractActivity
import com.vpigadas.lottery.random.number.utils.AreaDefinitionSingleton
import com.vpigadas.lottery.random.number.utils.AreaDefinitionSingleton.EXTRA_ARG_GAME

import kotlinx.android.synthetic.main.activity_second_screen.*
import kotlinx.android.synthetic.main.content_second_screen.*

class SecondScreen : AbstractActivity(), HoldersListener {

    override fun getResLayout(): Int = R.layout.activity_second_screen

    override fun initLayout() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val adapter= GamesAdapter(this)
        adapter.submitList(AreaDefinitionSingleton.listOfGames)

        recycler_view.adapter = adapter
    }

    override fun dismissUi() {

    }

    override fun onClickView(view: View) {
        val position = recycler_view.getChildAdapterPosition(view)

        val item = (recycler_view.adapter as GamesAdapter).getData(position)

        val intent = Intent(this, ThirdScreen::class.java).also { it.putExtra(EXTRA_ARG_GAME, item) }
        startActivity(intent)
    }

}
