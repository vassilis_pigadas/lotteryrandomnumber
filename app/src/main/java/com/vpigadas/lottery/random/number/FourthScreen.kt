package com.vpigadas.lottery.random.number

import android.view.View
import com.google.android.material.snackbar.Snackbar
import com.vpigadas.lottery.random.number.abstracts.AbstractActivity
import com.vpigadas.lottery.random.number.models.GameDefinition
import com.vpigadas.lottery.random.number.models.GeneratedModels
import com.vpigadas.lottery.random.number.utils.AreaDefinitionSingleton
import kotlinx.android.synthetic.main.activity_fourth_screen.*
import kotlinx.android.synthetic.main.content_fourth_screen.*
import java.util.*

class FourthScreen : AbstractActivity(), HoldersListener {

    override fun getResLayout(): Int = R.layout.activity_fourth_screen

    override fun initLayout() {
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val obj = intent.getParcelableExtra<GameDefinition>(AreaDefinitionSingleton.EXTRA_ARG_GAME)

        if (obj == null) {
            Snackbar.make(
                toolbar,
                getString(R.string.error_parcelable_obj),
                Snackbar.LENGTH_SHORT
            ).show()
        }

        toolbar.title = obj.getName(Locale.getDefault())

        val adapter = TicketsAdapter(this)

        val list = mutableListOf<GeneratedModels>()

        list.add(AreaDefinitionSingleton.generateTickets(obj.getName(Locale.getDefault())))
        list.add(AreaDefinitionSingleton.generateTickets(obj.getName(Locale.getDefault())))
        list.add(AreaDefinitionSingleton.generateTickets(obj.getName(Locale.getDefault())))

        adapter.submitList(list)

        recycler_fourth.adapter = adapter
    }

    override fun dismissUi() {}

    override fun onClickView(view: View) {
        val position = recycler_fourth.getChildAdapterPosition(view)

        val item = (recycler_fourth.adapter as TicketsAdapter).getData(position)

        Snackbar.make(
            toolbar,
            getString(R.string.generated_number_formatted, item.sum, item.odds, item.even),
            Snackbar.LENGTH_LONG
        ).show()
    }
}
