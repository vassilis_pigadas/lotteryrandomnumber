package com.vpigadas.lottery.random.number.utils

import com.vpigadas.lottery.random.number.models.AreaDefinition
import com.vpigadas.lottery.random.number.models.GameDefinition
import com.vpigadas.lottery.random.number.models.GeneratedModels
import java.util.*

object AreaDefinitionSingleton {
    const val EXTRA_ARG_GAME = "EXTRA_ARG_GAME"

    private val tzoker = GameDefinition(
        5000,
        "Tzoker",
        "Τζόκερ", listOf(
            AreaDefinition(5, 45),
            AreaDefinition(1, 20)
        )
    )

    private val lotto = GameDefinition(
        5001,
        "Lotto",
        "Λοττο", listOf(
            AreaDefinition(6, 49)
        )
    )

    val listOfGames: List<GameDefinition> = mutableListOf(tzoker, lotto)

    fun generateTickets(name: String): GeneratedModels = when (name) {
        tzoker.m_NameEn,
        tzoker.m_NameGr -> {
            generateTzokerNum()
        }
        lotto.m_NameEn,
        lotto.m_NameGr -> {
            generateLottoNum()
        }
        else -> generateTzokerNum()
    }

    fun generateTzokerNum(): GeneratedModels {
        val string = StringBuilder()

        var sum = 0
        var odd = 0
        var even = 0

        tzoker.m_AreaDefsArray.forEach {
            string.append("[")
            for (i in 1..it.m_NumbersToSelect) {
                if (i != 1) {
                    string.append(",")
                }
                val number = generateRandomInt(0, it.m_MaxNumbers)
                string.append(number)

                sum += number
                if (number % 2 == 0) {
                    odd += number
                } else {
                    even += number
                }
            }
            string.append("]")
        }

        return GeneratedModels(string.toString(), sum, odd, even)
    }

    fun generateLottoNum(): GeneratedModels {
        val string = StringBuilder()
        var sum = 0
        var odd = 0
        var even = 0

        lotto.m_AreaDefsArray.forEach {
            string.append("[")
            for (i in 1..it.m_NumbersToSelect) {
                if (i != 1) {
                    string.append(",")
                }
                val number = generateRandomInt(0, it.m_MaxNumbers)
                string.append(number)

                sum += number
                if (number % 2 == 0) {
                    odd += number
                } else {
                    even += number
                }
            }
            string.append("]")
        }

        return GeneratedModels(string.toString(), sum, odd, even)
    }

    private fun generateRandomInt(min: Int, max: Int): Int {
        val r = Random()
        return r.nextInt(max - min + 1) + min
    }
}