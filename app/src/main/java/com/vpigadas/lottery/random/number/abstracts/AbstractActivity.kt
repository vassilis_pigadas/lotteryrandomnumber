package com.vpigadas.lottery.random.number.abstracts

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.vpigadas.lottery.random.number.R

abstract class AbstractActivity : AppCompatActivity() {

    abstract fun getResLayout():Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getResLayout())
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        initLayout()
    }

    abstract fun initLayout()

    override fun onStop() {
        super.onStop()

        dismissUi()
    }

    abstract fun dismissUi()

}