package com.vpigadas.lottery.random.number

import android.view.View

interface HoldersListener {
    fun onClickView(view: View)
}