package com.vpigadas.lottery.random.number.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class GameDefinition(
    val m_GameCode: Int, //The unique game id
    val m_NameEn: String, //The English name of the game
    val m_NameGr: String, //The Greek name of the game
    val m_AreaDefsArray: List<AreaDefinition> //An array of AreaDefinition objects
) : Parcelable {

    fun getName(locale: Locale): String = when (locale.country) {
        "GR" -> m_NameGr
        else -> m_NameEn
    }
}

@Parcelize
data class AreaDefinition(
    val m_NumbersToSelect: Int,
    val m_MaxNumbers: Int
) : Parcelable

data class GeneratedModels(
    val formatted: String,
    val sum: Int,
    val odds: Int,
    val even: Int
)