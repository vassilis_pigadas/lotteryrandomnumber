package com.vpigadas.lottery.random.number

import android.content.Intent
import com.vpigadas.lottery.random.number.abstracts.AbstractActivity
import kotlinx.android.synthetic.main.content_main.*


class MainActivity : AbstractActivity() {
    override fun getResLayout(): Int = R.layout.activity_main

    override fun initLayout() {
        btn_picker.setOnClickListener { startActivity(Intent(this, SecondScreen::class.java)) }

        btn_cost.setOnClickListener {

        }
    }

    override fun dismissUi() {

    }
}
