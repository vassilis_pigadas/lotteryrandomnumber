package com.vpigadas.lottery.random.number

import android.view.View
import com.vpigadas.lottery.random.number.abstracts.AbstractViewHolder
import com.vpigadas.lottery.random.number.models.GeneratedModels
import kotlinx.android.synthetic.main.holder_game.view.*

class TicketsViewHolder(itemView: View) : AbstractViewHolder(itemView) {

    override fun bind(data: Any) {
        when (data) {
            is GeneratedModels -> {
                itemView.holder_game_title.text = data.formatted
            }
        }
    }
}