package com.vpigadas.lottery.random.number

import android.view.View
import com.vpigadas.lottery.random.number.abstracts.AbstractViewHolder
import kotlinx.android.synthetic.main.holder_game.view.*

class GameViewHolder(itemView: View) : AbstractViewHolder(itemView) {

    override fun bind(data: Any) {
        when (data) {
            is String -> {
                itemView.holder_game_title.text = data
            }
        }
    }
}