package com.vpigadas.lottery.random.number

import android.view.View
import com.vpigadas.lottery.random.number.abstracts.AbstractViewHolder

class EmptyViewHolder(itemView: View) : AbstractViewHolder(itemView) {
    override fun bind(data: Any) {

    }
}